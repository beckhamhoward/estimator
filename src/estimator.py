#!/usr/bin/env python2

import rospy
import math
import socket
import numpy as np
import time as clock
import LPF as lp
from bh_vision.msg import Field
from bh_motion_control.msg import Speed
from geometry_msgs.msg import Pose2D
from geometry_msgs.msg import Point
from bh_motion_control.msg import Speed

##############################################################################
##############################################################################

class Node():
    def __init__(self, mode):
        # Initialize the stored variables
        self.__setup()
        # This mode is for the LPF without measurement memory
        if mode == 2:
            self.__setupLPF()
        # This mode is for the EKF without measurement memory
        elif mode == 3:
            pass
        # This mode is for the LPF with measurement memory
        elif mode == 4:
            pass
        # This mode is for the EKF with measurement memory
        elif mode == 5:
            pass

	# Set up the ros node called 'estimator'
	rospy.init_node('estimator', anonymous=False)
	# Listen to the AI commands
	rospy.Subscriber('velocity', Pose2D, self.setCommandedVelocity)
	# Listen to the Vision data
	rospy.Subscriber('positions', Field, self.setField)
	# Listen to the Motion Control
	rospy.Subscriber('deadReckon_vel', Speed, self.setActualVelocity)
	# Publish to the Estimator
	pub = rospy.Publisher('positions_est', Field, queue_size=10)	
	# set up a tick rate for the estimator
	rate = rospy.Rate(20)	
	# Loop until the node is shutdown
	while not rospy.is_shutdown():
        # If the mode is set to pass through only
	    if mode == 1:
                # Call the pass through function
                self.passThrough()
            elif mode == 2:
                # Call the low Pass filter
                self.lowPassFilter()
            # Create a default case state that doesn't allow for higher modes
            else:
                self.passThrough()
            # Publish the estField to ROS
            pub.publish(self.estField)
            # Let the process sleep until the next call
            rate.sleep()
	
	##########################################################################
	# Function to setup and initialize the local variables
    def __setup(self):
        # set the actutual velocity to data type Pose2D
        self.actualVelocity    = Speed()
        # set the commanded velocity to data type Pose2D
        self.commandedVelocity = Pose2D()
        # set all of the field parameters to Pose2D
        self.estField          = Field()
        # When futureTime is zero then it will do no future prediction
        # When futureTime is negative it will not catch up to the actual time
        # When futureTime is positive it will try to predict the future
        self.futureTime        = [0, 0]
        # Set the field object of what the vision code is giving
        self.visField          = Field()
        # Keep track of when the velocity message was recieved
        self.velTimeOld        = 0
        # Keep track of the newest velocity change
        self.velTimeNew        = clock.time()
        # Keep track of when the old position was recieved
        self.posTimeOld        = 0
        # Keep Track of the newest position change
        self.posTimeNew        = clock.time()
        # Set who the robot is in order to correctly implement the filters
        hostname = socket.gethostname()
        if hostname == 'BECKHAM':
            self.isBeckham = 0
        elif hostname == 'HOWARD':
            self.isBeckham = 1
        else:
            self.isBeckham = 2
        self.trackTime = np.array([0.0, 0.0, 0.0, 0.0, 0.0])
    
    ##########################################################################
    ## Function to setup the LowPassFilterClass    
    def __setupLPF(self):
        # Call the low pass filter class
        self.estLPF = lp.LPF()

    ##########################################################################
    # set an array to a Pose2D
    def set_Pose2D(self, data):
        # Create an empty Pose2D
        temp = Pose2D()
        # Set the data x
        temp.x = data[0]
        # Set the data y
        temp.y = data[1]
        # set the data theta
        temp.theta = data[2]
        # return the Pose2D to the function that called set_Pose2D
        return temp

    ##########################################################################
    # set an array to a point
    def set_point(self, data):
        # Create an empty Point
        temp = Point()
        # set the data x
        temp.x = data[0]
        # set the data y
        temp.y = data[1]
        # set the data z
        temp.z = data[2]
        # return the point to the function that called set_point
        return temp

    ##########################################################################
    # Take a Pose2D data structure and convert to an array
    def set_array(self, data):
        # Return the array to the function that called set_array
        return np.array([data.x, data.y, data.theta])

    ##########################################################################
    # The ball is setup as a point so run this function for the ball
    def set_ball(self, data):
        # Return an array of the ball position
        return np.array([data.x, data.y, data.z])

    ##########################################################################
    # The low pass filter function
    def lowPassFilter(self):
        # Check to see what data is new always check the velocity first then
        # check the camera position.
        # If the velocity has been updtaed then adjust the position
        if self.actualVelocity.timestamp != self.velTimeOld:
            timeAdj = self.actualVelocity.timestamp - self.velTimeOld
            # Select the correct robot based on the local address if this code
            # is running on Howard then set the bool Beckham to False
            # Multiply the timeAdj by the velocity and then add it
            # to the position
            localVel = self.set_array(self.actualVelocity.velocity)
            if self.isBeckham == 0:
                # Send the velocity commands through the low pass filter
                pos = self.estLPF.lowPassFilterVel(localVel, timeAdj)
                # Pull out the x, y, and theta components and save them to the
                # estField variable to pass through to the AI node
                self.estField.home1 = self.set_Pose2D(pos)
            elif self.isBeckham == 1:
                # Send the velocity commands through the low pass filter
                pos = self.estLPF.lowPassFilterVel(localVel, timeAdj)
                # Pull out the x, y, and theta components and save them to the
                # estField variable to pass through to the AI node
                self.estField.home2 = self.set_Pose2D(pos)
            # This section will be used only for software testing only
            else:
                # Assuming that the information is comming in at 20 Hz.
                timeAdj = 0.050;
                # Assuming if we are running the code as if we are home1 
                # or Beckham
                # Send the velocity commands through the low pass filter
                pos = self.estLPF.lowPassFilterVel(localVel, timeAdj)
                # Pull out the x, y, and theta components and save them to the
                # estField variable to pass through to the AI node
                self.estField.home1 = self.set_Pose2D(pos)
            # Update the timestamp so we know that the velocity has been 
            # updated
            self.velTimeOld = self.actualVelocity.timestamp
        timeAdj = 0.05;  # Fix me
        # Send the new camera data through the low pass filter only if there
        # is new information available to use for home1
        if self.visField.hasHome1:
            # Set the estimator hasHome1 to be true
            self.estField.hasHome1 = True
            # Save the Pose2D to an array
            newPos = self.set_array(self.visField.home1)
            # Pass the new measurment into the low pass filter
            pos = self.estLPF.lowPassFilterPos(newPos)
            # Store the new position into the field
            self.setField.home1 = self.set_Pose2D(pos)
            # reset the tracking time
            self.trackTime[0] = 0.0
        # After a period of time if the object has not been seen then
        # set the bool variable to false signaling to the AI code that
        # that object is missing
        else:
            # Increment the time tracker array at the correct index
            self.trackTime += timeAdj
            # If the time exceeds a full second of time then set the 
            # boolean to false
            if self.trackTime[0] > 1.0:
                self.estField.hasHome1 = False

        # Send the new camera data through the low pass filter only if there 
        # is new information available to use for home2
        if self.visField.hasHome2:
            # Set the estimator hasHome2 to be true
            self.estField.hasHome2 = True
            # Save the Pose2D to an array
            newPos = self.set_array(self.visField.home2)
            # Pass the new measurment into the low pass filter
            pos = self.estLPF.lowPassFilterPos(newPos)
            # Store the new position into the field
            self.estField.home2 = self.set_Pose2D(pos)
            # Reset tracking time
            self.trackTime[1] = 0.0
        # After a period of time if the object has not been seen then
        # set the bool variable to false signaling to the AI code that
        # that object is missing
        else:
            # Increment the time tracker array at the correct index
            self.trackTime[1] += timeAdj
            # If the time exceeds a full second of time then set the 
            # boolean to false
            if self.trackTime[1] > 1.0:
                self.estField.hasHome2 = False

        if self.visField.hasAway1:
            # Set the estimator hasX to be true
            self.estField.hasAway1 = True
            # Save the Pose2D to an array
            newPos = self.set_array(self.visField.away1)
            # Pass the new measurment into the low pass filter
            pos = self.estLPF.lowPassFilterPos(newPos)
            # Store the new position into the field
            self.estField.away1 = self.set_Pose2D(pos)
            # Reset tracking time
            self.trackTime[2] = 0.0
        # After a period of time if the object has not been seen then
        # set the bool variable to false signaling to the AI code that
        # that object is missing
        else:
            # Increment the time tracker array at the correct index
            self.trackTime[2] += timeAdj
            # If the time exceeds a full second of time then set the 
            # boolean to false
            if self.trackTime[2] > 1.0:
                self.estField.hasAway1 = False

        if self.visField.hasAway2:
            # Set the estimator hasX to be true
            self.estField.hasAway2 = True
            # Save the Pose2D to an array
            newPos = self.set_array(self.visField.away2)
            # Pass the new measurment into the low pass filter
            pos = self.estLPF.lowPassFilterPos(newPos)
            # Store the new position into the field
            self.estField.away2 = self.set_Pose2D(pos)
            # Reset tracking time
            self.trackTime[3] = 0.0
        # After a period of time if the object has not been seen then
        # set the bool variable to false signaling to the AI code that
        # that object is missing
        else:
            # Increment the time tracker array at the correct index
            self.trackTime[3] += timeAdj
            # If the time exceeds a full second of time then set the 
            # boolean to false
            if self.trackTime[3] > 1.0:
                self.estField.hasAway2 = False

        if self.visField.hasBall:
            # Set the estimator hasX to be true
            self.estField.hasBall = True
            # Save the Pose2D to an array
            newPos = self.set_ball(self.visField.ball)
            # Pass the new measurment into the low pass filter
            pos = self.estLPF.lowPassFilterPos(newPos)
            # Store the new position into the field
            self.estField.ball = self.set_point(pos)
            # Reset tracking time
            self.trackTime[4] = 0.0
        # After a period of time if the object has not been seen then
        # set the bool variable to false signaling to the AI code that
        # that object is missing
        else:
            # Increment the time tracker array at the correct index
            self.trackTime[4] += timeAdj
            # If the time exceeds a full second of time then set the 
            # boolean to false
            if self.trackTime[4] > 1.0:
                self.estField.hasBall = False

    ##########################################################################
    # Set the field funciton
    def setField(self,data):
        self.visField = data
    
    ##########################################################################
    # Set the actual velocity based on the output from the motion control
    def setActualVelocity(self,data):
        self.actualVelocity = data
    
    ##########################################################################
    # Set the commanded velocity based on the output from the AI
    def setCommandedVelocity(self,data):
        self.commandedVelocity = data
    
    ##########################################################################
    # This function will take and pass through the information to the AI
    # with no alterations.
    def passThrough(self):
        # If the velocity has been updtaed then adjust the position
        if self.actualVelocity.timestamp != self.velTimeOld:
            timeAdj = self.actualVelocity.timestamp - self.velTimeOld
            # Select the correct robot based on the local address if this code
            # is running on Howard then set the bool Beckham to False
            # Multiply the timeAdj by the velocity and then add it
            # to the position
            localVel = np.array([self.actualVelocity.velocity.x,\
                self.actualVelocity.velocity.y,\
                self.actualVelocity.velocity.theta])
            if self.isBeckham == 0:
                self.estField.home1.x = self.estField.home1.x + timeAdj*\
                    self.actualVelocity.velocity.x
                self.estField.home1.y = self.estField.home1.y + timeAdj*\
                    self.actualVelocity.velocity.y
                self.estField.home1.theta = self.estField.home1.theta + \
                    timeAdj*self.actualVelocity.velocity.theta
            elif self.isBeckham == 1:
                self.estField.home2.x = self.estField.home2.x + timeAdj*\
                    self.actualVelocity.velocity.x
                self.estField.home2.y = self.estField.home2.y + timeAdj*\
                    self.actualVelocity.velocity.y
                self.estField.home2.theta = self.estField.home2.theta + \
                    timeAdj*self.actualVelocity.velocity.theta
            # This section will be used only for software testing only
            else:
                # Assuming that the information is comming in at 20 Hz.
                timeAdj = 0.050;
                # Assuming if we are running the code as if we are home1
                # or Beckham
                self.estField.home1.x = self.estField.home1.x + timeAdj*\
                    self.commandedVelocity.x
                self.estField.home1.y = self.estField.home1.y + timeAdj*\
                    self.commandedVelocity.y
                self.estField.home1.theta = self.estField.home1.theta + \
                    timeAdj*self.commandedVelocity.theta
            self.velTimeOld = self.actualVelocity.timestamp
        # Just set the estimator field to the vision field if there is new
        # information available to use
        ######################################################################
        ######################################################################
        timeAdj = 0.050;   # For now assume that we are running at 20 Hz
        ######################################################################
        ######################################################################
        if self.visField.hasHome1:
            self.estField.hasHome1 = True
            self.estField.home1 = self.visField.home1
            self.trackTime[0] = 0.0
        else:
            self.trackTime[0] += timeAdj
            if self.trackTime[0] > 1.0:
                self.estField.hasHome1 = False
            
        if self.visField.hasHome2:
            self.estField.hasHome2 = True
            self.estField.home2 = self.visField.home2
            self.trackTime[1] = 0.0
        else:
            self.trackTime[1] += timeAdj
            if self.trackTime[1] > 1.0:
                self.estField.hasHome2 = False

        if self.visField.hasAway1:
            self.estField.hasAway1 = True
            self.estField.away1 = self.visField.away1
            self.trackTime[2] = 0.0
        else:
            self.trackTime[2] += timeAdj
            if self.trackTime[2] > 1.0:
                self.estField.hasAway1 = False

        if self.visField.hasAway2:
            self.estField.hasAway2 = True
            self.estField.away2 = self.visField.away2
            self.trackTime[3] = 0.0
        else:
            self.trackTime[3] += timeAdj
            if self.trackTime[3] > 1.0:
                self.estField.hasAway2 = False

        if self.visField.hasBall:
            self.estField.hasBall = True
            self.estField.ball  = self.visField.ball
            self.trackTime[4] = 0.0
        else:
            self.trackTime[4] += timeAdj
            if self.trackTime[4] > 1.0:
                self.estField.hasBall = False
##############################################################################
# Set the control parameter to either:
#   Pass the Field:         1
#   Low Pass Filter:        2
#   Kalman Filter:          3
#   Low Pass Filter Future: 4
#   Kalman Filter:          5
MODE = 1

##############################################################################
##############################################################################

if __name__=="__main__":
    try:
        Node(MODE)
    except rospy.ROSInterruptException:
        pass
