#!/usr/bin/env python2

import math
import numpy as np
import time as clock
import socket

##############################################################################
##############################################################################
##############################################################################
##############################################################################
##############################################################################
## Define the alpha values to be looked up as an array based on the name
##############################################################################
##############################################################################
##############################################################################
##############################################################################
##############################################################################


##############################################################################
##############################################################################
# This is the low pass filter class that is used to smooth out the incoming
# measurements from both the camera and the roboclaws
class LPF():
	######################################################################
	# Set up the low pass filter
	def __init__(self):
		# Define the values of alpha for
		# x, y, theta, xdot, ydot, and thetadot
		self.alpha    = np.array([0.5, 0.5, 0.5, 0.5, 0.5, 0.5])
		self.x        = 0
		self.y        = 0
		self.theta    = 0
		self.xdot     = 0
		self.ydot     = 0
		self.thetadot = 0
	##########################################################################
	# A simple function to print out the data stats of the LPF
	def printStats(self):
		print "X: ", self.x, " Y: ", self.y, " Theta: ", self.theta
		print "Xdot: ", self.xdot, " Ydot: ", self.ydot, " Thetadot: ",\
			self.thetadot

	##########################################################################
	# This function will be called only if there is new camera data
	def lowPassFilterPos(self, newPos):
		self.x = self.x*self.alpha[0] + (1-self.alpha[0])*newPos[0]
		self.y = self.y*self.alpha[1] + (1-self.alpha[1])*newPos[1]
		self.theta = self.theta*self.alpha[2] + (1-self.alpha[2])*newPos[2]
		position = np.array([self.x, self.y, self.theta])
		# Estimate the velocity from the camera unless you are the robot that
		# is running
		return position
	##########################################################################
	# This function will only be called if there is new velocity data
	def lowPassFilterVel(self, newVel, time):
		self.xdot = self.xdot*self.alpha[3] + (1-self.alpha[3])*newVel[0]
		self.ydot = self.ydot*self.alpha[4] + (1-self.alpha[4])*newVel[1]
		self.thetadot = self.thetadot*self.alpha[5] + (1-self.alpha[5])*\
			newVel[2]
		self.x = self.x*self.alpha[0] + (1-self.alpha[0])*self.xdot*time
		self.y = self.y*self.alpha[1] + (1-self.alpha[1])*self.ydot*time
		self.theta = self.theta*self.alpha[2] + (1-self.alpha[2])*\
			self.thetadot*time
		position = np.array([self.x, self.y, self.theta])
		return position


##############################################################################
##############################################################################
# This main is used for testing the LPF to make sure the math is doing what
# it should be doing
if __name__ == "__main__":
	testLPF = LPF()
	testLPF.printStats()
	pos = np.array([1,2,3])
	testLPF.lowPassFilterPos(pos)
	testLPF.printStats()
	testLPF.lowPassFilterVel(pos, 4)
	testLPF.printStats()
