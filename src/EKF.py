#!/usr/bin/env python2

import numpy as np
import socket
import LPF as lp
import math

##############################################################################
##############################################################################

# Parameters that I have to pass into my EKF
	# Vision data: time is included
	# Motor Speeds: time is included
	# Commanded Speeds: time is not included

# Parameters that I need to keep track of
	# Old position's
	# Old velocity commands -> this is an array of velocity
	# Q -> Is a value that I need to tune to make the robot
		# work.

# Algorithm
	# If we start up the function
		# Set xhat to starting position
		# Set the xhat_dl to starting position
		# Set the S parameters to be the following
			# P.field_width/2
			# P.field_width/2
			# (5*pi/180)^2
		# Set the initial velocity commanded to zero
		# Set the velocity delayed to zero
	# Else
		# Shift old velocity commands
		# Save new old velocity command
	# End

	# Prediction step
		# For N times
			# xhat = xhat + P.control_sample_rate/N*v_command
			# S = S + P.control_sample_rate/N*P.Q

	# Correction step
		# Doesn't compensate for the camera delay
			# y = camera position
			# y_pred = xhat
			# L = S/(P.R_ownteam+S)
			# S = eye(3)-L*S
			# xhat = xhat + L*(y-y_pred)
		# Compensates for the camera delay
			# y = camera position
			# y_pred = xhat_dl
			# L = S_dl/(P.R+S_dl)
			# S_dl = I - L*S_dl
			# xhat_dl = xhat_dl + L*(y-y_pred)
			# for N*max_delay_idx
				# xhat_dl = xhat_dl + P.control_sample_rate/N*\
					# v_command_dl
				# S_dl = S_dl + P.control_sample_rate/N*Q
			# xhat = xhat_dl
			# S = S_dl

##############################################################################
##############################################################################
# Constants used for the EKF class
FIELD_WIDTH     = 10 				# How wide is the field
ANGLE_VARIANCE  = 5*math.pi/180 	# Assume a X degree variance of position
VELOCITY_MEMORY = 10 				# How many velocity commands to save


##############################################################################
##############################################################################
# This is the Estimated Kalman Filter class
class EKF():
	##########################################################################
	# Run the init function
	def __init__(self, name):
		# Call the setup function to save the name and also set Q based on the
		# predefined and tuned parameters.
		self.Q           = self.__setup(name)
		# Set xhat based on the x, y, and theta positions of the ball
		self.xhat        = np.array([0.0,0.0,0.0])
		# Set xhat_dl for x, y, and theta postions of the ball
		self.xhat_dl     = np.array([0.0,0.0,0.0])
		# Create a matrix S with the starting parameters
		self.S           = np.diag([FIELD_WIDTH/2,FIELD_WIDTH/2,\
			ANGLE_VARIANCE])
		self.S_dl        = self.S
		self.velocity_dl = np.matrix('0.0;0.0;0.0')
		for size in range (1,VELOCITY_MEMORY):
			temp = np.matrix('0.0;0.0;0.0')
			self.velocity_dl = np.concatenate((self.velocity_dl, temp), 1)

	##########################################################################
	# Initialize variables
	def __setup(self, name):
		self.name = name;
		# Return the value of Q to the function that called this
		return np.matrix('1,0,0;0,1,0;0,0,1')
	
	##########################################################################
	# If there is a position measurement then update
	def newPositionUpdate(self, newPos):
		pass

	##########################################################################
	# If there is a velocity measurement then update
	def newVelocityUpdtate(self, newVel):
		pass

	##########################################################################
	# If there are no updates move the prediction
	def update(self):
		pass

	##########################################################################
	# Print out the stats to see how the estimator is working
	def printStats(self):
		print(self.name)
		print "States: ", self.xhat
		print "States_delayed: ", self.xhat_dl
		print "S:\n", self.S
		print "S_dl:\n", self.S_dl
		print "velocity_dl:\n", self.velocity_dl
		print "Q: \n", self.Q


##############################################################################
##############################################################################
# This main function is used for testing the EKF to see if it functions
# properly
if __name__ == "__main__":
	test = EKF("Test")
	test.printStats()